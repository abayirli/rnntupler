// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME TRTFrameworkCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "TRTFramework/Config.h"
#include "TRTFramework/TNPAlgorithm.h"
#include "TRTFramework/Algorithm.h"
#include "TRTFramework/TRTFrameworkDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static TClass *xTRTcLcLConfig_Dictionary();
   static void xTRTcLcLConfig_TClassManip(TClass*);
   static void *new_xTRTcLcLConfig(void *p = 0);
   static void *newArray_xTRTcLcLConfig(Long_t size, void *p);
   static void delete_xTRTcLcLConfig(void *p);
   static void deleteArray_xTRTcLcLConfig(void *p);
   static void destruct_xTRTcLcLConfig(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xTRT::Config*)
   {
      ::xTRT::Config *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::xTRT::Config));
      static ::ROOT::TGenericClassInfo 
         instance("xTRT::Config", "TRTFramework/Config.h", 28,
                  typeid(::xTRT::Config), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &xTRTcLcLConfig_Dictionary, isa_proxy, 4,
                  sizeof(::xTRT::Config) );
      instance.SetNew(&new_xTRTcLcLConfig);
      instance.SetNewArray(&newArray_xTRTcLcLConfig);
      instance.SetDelete(&delete_xTRTcLcLConfig);
      instance.SetDeleteArray(&deleteArray_xTRTcLcLConfig);
      instance.SetDestructor(&destruct_xTRTcLcLConfig);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xTRT::Config*)
   {
      return GenerateInitInstanceLocal((::xTRT::Config*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::xTRT::Config*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *xTRTcLcLConfig_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::xTRT::Config*)0x0)->GetClass();
      xTRTcLcLConfig_TClassManip(theClass);
   return theClass;
   }

   static void xTRTcLcLConfig_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_xTRTcLcLAlgorithm(void *p = 0);
   static void *newArray_xTRTcLcLAlgorithm(Long_t size, void *p);
   static void delete_xTRTcLcLAlgorithm(void *p);
   static void deleteArray_xTRTcLcLAlgorithm(void *p);
   static void destruct_xTRTcLcLAlgorithm(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xTRT::Algorithm*)
   {
      ::xTRT::Algorithm *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xTRT::Algorithm >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xTRT::Algorithm", ::xTRT::Algorithm::Class_Version(), "TRTFramework/Algorithm.h", 41,
                  typeid(::xTRT::Algorithm), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::xTRT::Algorithm::Dictionary, isa_proxy, 4,
                  sizeof(::xTRT::Algorithm) );
      instance.SetNew(&new_xTRTcLcLAlgorithm);
      instance.SetNewArray(&newArray_xTRTcLcLAlgorithm);
      instance.SetDelete(&delete_xTRTcLcLAlgorithm);
      instance.SetDeleteArray(&deleteArray_xTRTcLcLAlgorithm);
      instance.SetDestructor(&destruct_xTRTcLcLAlgorithm);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xTRT::Algorithm*)
   {
      return GenerateInitInstanceLocal((::xTRT::Algorithm*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::xTRT::Algorithm*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_xTRTcLcLTNPAlgorithm(void *p = 0);
   static void *newArray_xTRTcLcLTNPAlgorithm(Long_t size, void *p);
   static void delete_xTRTcLcLTNPAlgorithm(void *p);
   static void deleteArray_xTRTcLcLTNPAlgorithm(void *p);
   static void destruct_xTRTcLcLTNPAlgorithm(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xTRT::TNPAlgorithm*)
   {
      ::xTRT::TNPAlgorithm *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xTRT::TNPAlgorithm >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xTRT::TNPAlgorithm", ::xTRT::TNPAlgorithm::Class_Version(), "TRTFramework/TNPAlgorithm.h", 20,
                  typeid(::xTRT::TNPAlgorithm), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::xTRT::TNPAlgorithm::Dictionary, isa_proxy, 4,
                  sizeof(::xTRT::TNPAlgorithm) );
      instance.SetNew(&new_xTRTcLcLTNPAlgorithm);
      instance.SetNewArray(&newArray_xTRTcLcLTNPAlgorithm);
      instance.SetDelete(&delete_xTRTcLcLTNPAlgorithm);
      instance.SetDeleteArray(&deleteArray_xTRTcLcLTNPAlgorithm);
      instance.SetDestructor(&destruct_xTRTcLcLTNPAlgorithm);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xTRT::TNPAlgorithm*)
   {
      return GenerateInitInstanceLocal((::xTRT::TNPAlgorithm*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::xTRT::TNPAlgorithm*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace xTRT {
//______________________________________________________________________________
atomic_TClass_ptr Algorithm::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Algorithm::Class_Name()
{
   return "xTRT::Algorithm";
}

//______________________________________________________________________________
const char *Algorithm::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::Algorithm*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Algorithm::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::Algorithm*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Algorithm::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::Algorithm*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Algorithm::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::Algorithm*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xTRT
namespace xTRT {
//______________________________________________________________________________
atomic_TClass_ptr TNPAlgorithm::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *TNPAlgorithm::Class_Name()
{
   return "xTRT::TNPAlgorithm";
}

//______________________________________________________________________________
const char *TNPAlgorithm::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::TNPAlgorithm*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int TNPAlgorithm::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::TNPAlgorithm*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *TNPAlgorithm::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::TNPAlgorithm*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *TNPAlgorithm::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::TNPAlgorithm*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xTRT
namespace ROOT {
   // Wrappers around operator new
   static void *new_xTRTcLcLConfig(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::xTRT::Config : new ::xTRT::Config;
   }
   static void *newArray_xTRTcLcLConfig(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::xTRT::Config[nElements] : new ::xTRT::Config[nElements];
   }
   // Wrapper around operator delete
   static void delete_xTRTcLcLConfig(void *p) {
      delete ((::xTRT::Config*)p);
   }
   static void deleteArray_xTRTcLcLConfig(void *p) {
      delete [] ((::xTRT::Config*)p);
   }
   static void destruct_xTRTcLcLConfig(void *p) {
      typedef ::xTRT::Config current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xTRT::Config

namespace xTRT {
//______________________________________________________________________________
void Algorithm::Streamer(TBuffer &R__b)
{
   // Stream an object of class xTRT::Algorithm.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xTRT::Algorithm::Class(),this);
   } else {
      R__b.WriteClassBuffer(xTRT::Algorithm::Class(),this);
   }
}

} // namespace xTRT
namespace ROOT {
   // Wrappers around operator new
   static void *new_xTRTcLcLAlgorithm(void *p) {
      return  p ? new(p) ::xTRT::Algorithm : new ::xTRT::Algorithm;
   }
   static void *newArray_xTRTcLcLAlgorithm(Long_t nElements, void *p) {
      return p ? new(p) ::xTRT::Algorithm[nElements] : new ::xTRT::Algorithm[nElements];
   }
   // Wrapper around operator delete
   static void delete_xTRTcLcLAlgorithm(void *p) {
      delete ((::xTRT::Algorithm*)p);
   }
   static void deleteArray_xTRTcLcLAlgorithm(void *p) {
      delete [] ((::xTRT::Algorithm*)p);
   }
   static void destruct_xTRTcLcLAlgorithm(void *p) {
      typedef ::xTRT::Algorithm current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xTRT::Algorithm

namespace xTRT {
//______________________________________________________________________________
void TNPAlgorithm::Streamer(TBuffer &R__b)
{
   // Stream an object of class xTRT::TNPAlgorithm.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xTRT::TNPAlgorithm::Class(),this);
   } else {
      R__b.WriteClassBuffer(xTRT::TNPAlgorithm::Class(),this);
   }
}

} // namespace xTRT
namespace ROOT {
   // Wrappers around operator new
   static void *new_xTRTcLcLTNPAlgorithm(void *p) {
      return  p ? new(p) ::xTRT::TNPAlgorithm : new ::xTRT::TNPAlgorithm;
   }
   static void *newArray_xTRTcLcLTNPAlgorithm(Long_t nElements, void *p) {
      return p ? new(p) ::xTRT::TNPAlgorithm[nElements] : new ::xTRT::TNPAlgorithm[nElements];
   }
   // Wrapper around operator delete
   static void delete_xTRTcLcLTNPAlgorithm(void *p) {
      delete ((::xTRT::TNPAlgorithm*)p);
   }
   static void deleteArray_xTRTcLcLTNPAlgorithm(void *p) {
      delete [] ((::xTRT::TNPAlgorithm*)p);
   }
   static void destruct_xTRTcLcLTNPAlgorithm(void *p) {
      typedef ::xTRT::TNPAlgorithm current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xTRT::TNPAlgorithm

namespace ROOT {
   static TClass *vectorlEstringgR_Dictionary();
   static void vectorlEstringgR_TClassManip(TClass*);
   static void *new_vectorlEstringgR(void *p = 0);
   static void *newArray_vectorlEstringgR(Long_t size, void *p);
   static void delete_vectorlEstringgR(void *p);
   static void deleteArray_vectorlEstringgR(void *p);
   static void destruct_vectorlEstringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<string>*)
   {
      vector<string> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<string>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<string>", -2, "vector", 339,
                  typeid(vector<string>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEstringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<string>) );
      instance.SetNew(&new_vectorlEstringgR);
      instance.SetNewArray(&newArray_vectorlEstringgR);
      instance.SetDelete(&delete_vectorlEstringgR);
      instance.SetDeleteArray(&deleteArray_vectorlEstringgR);
      instance.SetDestructor(&destruct_vectorlEstringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<string> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<string>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEstringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<string>*)0x0)->GetClass();
      vectorlEstringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEstringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEstringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string> : new vector<string>;
   }
   static void *newArray_vectorlEstringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<string>[nElements] : new vector<string>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEstringgR(void *p) {
      delete ((vector<string>*)p);
   }
   static void deleteArray_vectorlEstringgR(void *p) {
      delete [] ((vector<string>*)p);
   }
   static void destruct_vectorlEstringgR(void *p) {
      typedef vector<string> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<string>

namespace {
  void TriggerDictionaryInitialization_libTRTFramework_Impl() {
    static const char* headers[] = {
"TRTFramework/Config.h",
"TRTFramework/TNPAlgorithm.h",
"TRTFramework/Algorithm.h",
"TRTFramework/TRTFrameworkDict.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/build/TRTFramework/CMakeFiles/makeTRTFrameworkCintDict.kEYTcT/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libTRTFramework dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xTRT{class __attribute__((annotate("$clingAutoload$TRTFramework/Config.h")))  Config;}
namespace xTRT{class __attribute__((annotate("$clingAutoload$TRTFramework/Algorithm.h")))  __attribute__((annotate("$clingAutoload$TRTFramework/TNPAlgorithm.h")))  Algorithm;}
namespace xTRT{class __attribute__((annotate("$clingAutoload$TRTFramework/TNPAlgorithm.h")))  TNPAlgorithm;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libTRTFramework dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "TRTFramework-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ TRTFramework-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "TRTFramework/Config.h"
#include "TRTFramework/TNPAlgorithm.h"
#include "TRTFramework/Algorithm.h"
#include "TRTFramework/TRTFrameworkDict.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xTRT::Algorithm", payloadCode, "@",
"xTRT::Config", payloadCode, "@",
"xTRT::TNPAlgorithm", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libTRTFramework",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libTRTFramework_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libTRTFramework_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libTRTFramework() {
  TriggerDictionaryInitialization_libTRTFramework_Impl();
}
