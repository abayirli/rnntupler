file(REMOVE_RECURSE
  "../x86_64-centos7-gcc8-opt/include/TRTFramework"
  "../x86_64-centos7-gcc8-opt/data/TRTFramework/default.cfg"
  "../x86_64-centos7-gcc8-opt/data/TRTFramework/sample_list.txt"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/Package_TRTFramework_tests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
