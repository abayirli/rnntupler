#----------------------------------------------------------------
# Generated CMake target import file for configuration "RelWithDebInfo".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "TRTNTupler::MCDNTupler" for configuration "RelWithDebInfo"
set_property(TARGET TRTNTupler::MCDNTupler APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(TRTNTupler::MCDNTupler PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libMCDNTupler.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libMCDNTupler.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS TRTNTupler::MCDNTupler )
list(APPEND _IMPORT_CHECK_FILES_FOR_TRTNTupler::MCDNTupler "${_IMPORT_PREFIX}/lib/libMCDNTupler.so" )

# Import target "TRTNTupler::runNTuplerAlg" for configuration "RelWithDebInfo"
set_property(TARGET TRTNTupler::runNTuplerAlg APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(TRTNTupler::runNTuplerAlg PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/bin/runNTuplerAlg"
  )

list(APPEND _IMPORT_CHECK_TARGETS TRTNTupler::runNTuplerAlg )
list(APPEND _IMPORT_CHECK_FILES_FOR_TRTNTupler::runNTuplerAlg "${_IMPORT_PREFIX}/bin/runNTuplerAlg" )

# Import target "TRTNTupler::TRTFramework" for configuration "RelWithDebInfo"
set_property(TARGET TRTNTupler::TRTFramework APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(TRTNTupler::TRTFramework PROPERTIES
  IMPORTED_LOCATION_RELWITHDEBINFO "${_IMPORT_PREFIX}/lib/libTRTFramework.so"
  IMPORTED_SONAME_RELWITHDEBINFO "libTRTFramework.so"
  )

list(APPEND _IMPORT_CHECK_TARGETS TRTNTupler::TRTFramework )
list(APPEND _IMPORT_CHECK_FILES_FOR_TRTNTupler::TRTFramework "${_IMPORT_PREFIX}/lib/libTRTFramework.so" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
