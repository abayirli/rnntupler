// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME MCDNTuplerCintDict

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "MCDNTupler/NTuplerBase.h"
#include "MCDNTupler/NTuplerAlg.h"
#include "MCDNTupler/MCDNTuplerDict.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void *new_xTRTcLcLNTuplerBase(void *p = 0);
   static void *newArray_xTRTcLcLNTuplerBase(Long_t size, void *p);
   static void delete_xTRTcLcLNTuplerBase(void *p);
   static void deleteArray_xTRTcLcLNTuplerBase(void *p);
   static void destruct_xTRTcLcLNTuplerBase(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xTRT::NTuplerBase*)
   {
      ::xTRT::NTuplerBase *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xTRT::NTuplerBase >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xTRT::NTuplerBase", ::xTRT::NTuplerBase::Class_Version(), "MCDNTupler/NTuplerBase.h", 11,
                  typeid(::xTRT::NTuplerBase), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::xTRT::NTuplerBase::Dictionary, isa_proxy, 4,
                  sizeof(::xTRT::NTuplerBase) );
      instance.SetNew(&new_xTRTcLcLNTuplerBase);
      instance.SetNewArray(&newArray_xTRTcLcLNTuplerBase);
      instance.SetDelete(&delete_xTRTcLcLNTuplerBase);
      instance.SetDeleteArray(&deleteArray_xTRTcLcLNTuplerBase);
      instance.SetDestructor(&destruct_xTRTcLcLNTuplerBase);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xTRT::NTuplerBase*)
   {
      return GenerateInitInstanceLocal((::xTRT::NTuplerBase*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::xTRT::NTuplerBase*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_xTRTcLcLNTuplerAlg(void *p = 0);
   static void *newArray_xTRTcLcLNTuplerAlg(Long_t size, void *p);
   static void delete_xTRTcLcLNTuplerAlg(void *p);
   static void deleteArray_xTRTcLcLNTuplerAlg(void *p);
   static void destruct_xTRTcLcLNTuplerAlg(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::xTRT::NTuplerAlg*)
   {
      ::xTRT::NTuplerAlg *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::xTRT::NTuplerAlg >(0);
      static ::ROOT::TGenericClassInfo 
         instance("xTRT::NTuplerAlg", ::xTRT::NTuplerAlg::Class_Version(), "MCDNTupler/NTuplerAlg.h", 10,
                  typeid(::xTRT::NTuplerAlg), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::xTRT::NTuplerAlg::Dictionary, isa_proxy, 4,
                  sizeof(::xTRT::NTuplerAlg) );
      instance.SetNew(&new_xTRTcLcLNTuplerAlg);
      instance.SetNewArray(&newArray_xTRTcLcLNTuplerAlg);
      instance.SetDelete(&delete_xTRTcLcLNTuplerAlg);
      instance.SetDeleteArray(&deleteArray_xTRTcLcLNTuplerAlg);
      instance.SetDestructor(&destruct_xTRTcLcLNTuplerAlg);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::xTRT::NTuplerAlg*)
   {
      return GenerateInitInstanceLocal((::xTRT::NTuplerAlg*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::xTRT::NTuplerAlg*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace xTRT {
//______________________________________________________________________________
atomic_TClass_ptr NTuplerBase::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *NTuplerBase::Class_Name()
{
   return "xTRT::NTuplerBase";
}

//______________________________________________________________________________
const char *NTuplerBase::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerBase*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int NTuplerBase::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerBase*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *NTuplerBase::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerBase*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *NTuplerBase::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerBase*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xTRT
namespace xTRT {
//______________________________________________________________________________
atomic_TClass_ptr NTuplerAlg::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *NTuplerAlg::Class_Name()
{
   return "xTRT::NTuplerAlg";
}

//______________________________________________________________________________
const char *NTuplerAlg::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerAlg*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int NTuplerAlg::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerAlg*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *NTuplerAlg::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerAlg*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *NTuplerAlg::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::xTRT::NTuplerAlg*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace xTRT
namespace xTRT {
//______________________________________________________________________________
void NTuplerBase::Streamer(TBuffer &R__b)
{
   // Stream an object of class xTRT::NTuplerBase.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xTRT::NTuplerBase::Class(),this);
   } else {
      R__b.WriteClassBuffer(xTRT::NTuplerBase::Class(),this);
   }
}

} // namespace xTRT
namespace ROOT {
   // Wrappers around operator new
   static void *new_xTRTcLcLNTuplerBase(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::xTRT::NTuplerBase : new ::xTRT::NTuplerBase;
   }
   static void *newArray_xTRTcLcLNTuplerBase(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::xTRT::NTuplerBase[nElements] : new ::xTRT::NTuplerBase[nElements];
   }
   // Wrapper around operator delete
   static void delete_xTRTcLcLNTuplerBase(void *p) {
      delete ((::xTRT::NTuplerBase*)p);
   }
   static void deleteArray_xTRTcLcLNTuplerBase(void *p) {
      delete [] ((::xTRT::NTuplerBase*)p);
   }
   static void destruct_xTRTcLcLNTuplerBase(void *p) {
      typedef ::xTRT::NTuplerBase current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xTRT::NTuplerBase

namespace xTRT {
//______________________________________________________________________________
void NTuplerAlg::Streamer(TBuffer &R__b)
{
   // Stream an object of class xTRT::NTuplerAlg.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(xTRT::NTuplerAlg::Class(),this);
   } else {
      R__b.WriteClassBuffer(xTRT::NTuplerAlg::Class(),this);
   }
}

} // namespace xTRT
namespace ROOT {
   // Wrappers around operator new
   static void *new_xTRTcLcLNTuplerAlg(void *p) {
      return  p ? new(p) ::xTRT::NTuplerAlg : new ::xTRT::NTuplerAlg;
   }
   static void *newArray_xTRTcLcLNTuplerAlg(Long_t nElements, void *p) {
      return p ? new(p) ::xTRT::NTuplerAlg[nElements] : new ::xTRT::NTuplerAlg[nElements];
   }
   // Wrapper around operator delete
   static void delete_xTRTcLcLNTuplerAlg(void *p) {
      delete ((::xTRT::NTuplerAlg*)p);
   }
   static void deleteArray_xTRTcLcLNTuplerAlg(void *p) {
      delete [] ((::xTRT::NTuplerAlg*)p);
   }
   static void destruct_xTRTcLcLNTuplerAlg(void *p) {
      typedef ::xTRT::NTuplerAlg current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::xTRT::NTuplerAlg

namespace {
  void TriggerDictionaryInitialization_libMCDNTupler_Impl() {
    static const char* headers[] = {
"MCDNTupler/NTuplerBase.h",
"MCDNTupler/NTuplerAlg.h",
"MCDNTupler/MCDNTuplerDict.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/MCDNTupler",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/MCDNTupler",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/MCDNTupler",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/MCDNTupler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/TRTNTupler/TRTFramework",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgTools",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthToolSupport/AsgMessaging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccessInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/xAODRootAccess",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthContainersInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/AthLinksSA",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Control/CxxUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventFormat",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DataQuality/GoodRunsLists",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEventInfo",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/Interfaces/AsgAnalysisInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBase",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/AnaAlgorithm",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/RootCoreUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PileupReweighting",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigDecisionInterface",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/MCTruthClassifier",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODCaloEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Calorimeter/CaloGeoHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/GeoPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/EventPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODEgamma",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPrimitives",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTracking",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include/eigen3",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTruth",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODJet",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODBTagging",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODMuon",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/MuonSpectrometer/MuonIdHelpers",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODPFlow",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Event/xAOD/xAODTrigger",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Generators/TruthUtils",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoop",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/SampleHandler",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopAlgs",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/MultiDraw",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/D3PDTools/EventLoopGrid",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/InnerDetector/InDetRecTools/InDetTrackSelectionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/PhysicsAnalysis/AnalysisCommon/PATCore",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfxAOD",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfL1Data",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfHLTData",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigConfiguration/TrigConfInterfaces",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TrigDecisionTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigNavStructure",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigRoiConversion",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigEvent/TrigSteeringEvent",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/RoiDescriptor",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/../../../../AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/DetectorDescription/IRegionSelector",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Trigger/TrigAnalysis/TriggerMatchingTool",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/src/Tools/PathResolver",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBase/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/RootCore/include",
"/cvmfs/atlas.cern.ch/repo/sw/software/21.2/AnalysisBaseExternals/21.2.100/InstallArea/x86_64-centos7-gcc8-opt/include",
"/afs/cern.ch/user/a/abayirli/PhilippeNtupler/build/MCDNTupler/CMakeFiles/makeMCDNTuplerCintDict.yse6S3/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "libMCDNTupler dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace xTRT{class __attribute__((annotate("$clingAutoload$MCDNTupler/NTuplerBase.h")))  NTuplerBase;}
namespace xTRT{class __attribute__((annotate("$clingAutoload$MCDNTupler/NTuplerAlg.h")))  NTuplerAlg;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "libMCDNTupler dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef HAVE_PRETTY_FUNCTION
  #define HAVE_PRETTY_FUNCTION 1
#endif
#ifndef HAVE_64_BITS
  #define HAVE_64_BITS 1
#endif
#ifndef __IDENTIFIER_64BIT__
  #define __IDENTIFIER_64BIT__ 1
#endif
#ifndef ATLAS
  #define ATLAS 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 25
#endif
#ifndef PACKAGE_VERSION
  #define PACKAGE_VERSION "MCDNTupler-00-00-00"
#endif
#ifndef PACKAGE_VERSION_UQ
  #define PACKAGE_VERSION_UQ MCDNTupler-00-00-00
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif
#ifndef USE_CMAKE
  #define USE_CMAKE 1
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "MCDNTupler/NTuplerBase.h"
#include "MCDNTupler/NTuplerAlg.h"
#include "MCDNTupler/MCDNTuplerDict.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"xTRT::NTuplerAlg", payloadCode, "@",
"xTRT::NTuplerBase", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("libMCDNTupler",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_libMCDNTupler_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_libMCDNTupler_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_libMCDNTupler() {
  TriggerDictionaryInitialization_libMCDNTupler_Impl();
}
